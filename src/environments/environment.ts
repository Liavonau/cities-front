
export const environment = {
  production: false,
  apiBaseUrl: 'http://localhost:8080/api',
  baseUrl: 'http://localhost:4200',
  idleTimeoutEnabled: false
};
