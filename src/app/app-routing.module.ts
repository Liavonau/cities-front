import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityCardComponent } from './city-card/city-card.component';
import { CityComponent } from './city/city.component';

const routes: Routes = [
  {path: '', component: CityComponent},
  {path: ':id', component: CityCardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
