import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CityCardComponent } from './city-card.component';



@NgModule({
  declarations: [
    CityCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class CityCardModule { }
