import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { ICity } from '../shared/models/city.model';
import { CityService } from '../shared/services/city.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.css']
})
export class CityCardComponent {

  city: ICity;
  name: string;
  photo: string;

  constructor(private cityService: CityService, private activateRoute: ActivatedRoute, private modalService: NgbModal) {
  }

  ngOnInit(){
    this.activateRoute.paramMap.subscribe((obs) => {
      var idParam = obs.get('id')
      if (idParam) {
        var id = Number.parseInt(idParam);
        this.cityService.getCity(id).subscribe(data => {
          this.city = data;
          this.name = this.city.name;
          this.photo = this.city.photo;
        });
      }
    });
  }

  onSubmit() {
    const modalRef = this.modalService.open(NgbdModalConfirm, { size: 'lg' });
    modalRef.componentInstance.city = {
      id: this.city.id,
      name: this.name,
      photo: this.photo
    };
  }
}

@Component({
  selector: 'ngbd-modal-confirm',
  standalone: true,
  template: `
		<div class="modal-header">
			<h4 class="modal-title" id="modal-title">Edit city</h4>
			<button
				type="button"
				class="btn-close"
				aria-describedby="modal-title"
				(click)="modal.dismiss('Cross click')"
			></button>
		</div>
		<div class="modal-body">
			<p>
				<strong>Are you sure you want to update the city to {{city.name}}?</strong>
			</p>
			<p>
				City information will be changed.
				<span class="text-danger">This operation can not be undone.</span>
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline-secondary" (click)="modal.dismiss('cancel click')">Cancel</button>
			<button type="button" class="btn btn-primary" (click)="onSubmit()">Ok</button>
		</div>
	`,
})
export class NgbdModalConfirm {
  city: ICity;
  name: string;
  constructor(public modal: NgbActiveModal, private cityService: CityService, private router: Router) {}

  onSubmit() {
    this.modal.close('Ok click');
    this.cityService.updateCity(this.city).subscribe(data => {
      this.router.navigateByUrl('/');
    });
  }
}

