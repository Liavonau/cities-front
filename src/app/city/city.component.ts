import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { ICity } from '../shared/models/city.model';
import { CityService } from '../shared/services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit{

  cities: ICity[] = new Array;
  search: string = '';
  page: any;
  currentPage: number = 1;
  pageSize: number;
  totalElements: number;

  constructor(private cityService: CityService) {
  }

  ngOnInit(): void {
    this.loadCities();
  }

  onSearchChange(): void {
    this.currentPage = 1;
    this.loadCities();
  }

  onChangePage(pageNumber: any) {
    this.currentPage = pageNumber;
    this.loadCities();
  }

  loadCities() {
    this.cityService.getCities(this.currentPage - 1, this.search).subscribe(data => {
      this.cities = data.content;
      this.totalElements = data.totalElements;
      this.pageSize = data.size;
    });
  }
}

