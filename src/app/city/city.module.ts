import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityComponent } from './city.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CityComponent
  ],
  imports: [
    CommonModule, FormsModule, NgbModule, RouterModule
  ]
})
export class CityModule { }
