import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { IPage } from '../models/common.model';
import { ICity } from '../models/city.model';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) { }

  getCities(page: number, search: string): Observable<IPage<ICity>> {
    const requestParams = new HttpParams();
    const options = {
      params: requestParams.set('page', page).set('search', search),
    };
    return this.http.get<IPage<ICity>>(`${environment.apiBaseUrl}/cities`, options);
  }

  getCity(id: number): Observable<ICity> {
    return this.http.get<ICity>(`${environment.apiBaseUrl}/cities/${id}`);
  }

  updateCity(city: ICity): Observable<ICity> {
    return this.http.put<ICity>(`${environment.apiBaseUrl}/cities`, city);
  }
}
